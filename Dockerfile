########################################################################
# BUILD STAGE 1:
#
# A basic ubuntu image, that we only use to clone the PAL/INRIA gitlab
# repos. This image contains a layer with my private SSH key, but the image
# is never pushed to a public registry.
#
FROM ubuntu:latest as sources

ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
# ssh is used to test GitHub access
RUN apt-get update && apt-get -y install ssh git

# attention, the key MUST be in the docker build dir
ARG SSH_KEY=docker_id_rsa
ADD ${SSH_KEY} /root/.ssh/id_rsa
ENV GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

#RUN git clone --depth=1 https://github.com/ros4hri/hri_face_detect.git
#RUN git clone --depth=1 https://github.com/ros4hri/hri_fullbody.git -b no_depth_body_position
#RUN git clone --depth=1 git@gitlab.inria.fr:spring/wp7_ari/hri_face_identification.git

ARG cache_bust=4
RUN git clone --depth=1 --branch spring git@gitlab.inria.fr:spring/wp7_ari/libhri.git hri
RUN git clone --depth=1 git@gitlab.inria.fr:spring/wp7_ari/hri_person_manager.git
RUN git clone --depth=1 git@gitlab.inria.fr:spring/wp7_ari/knowledge_core.git
RUN git clone --depth=1 git@gitlab.inria.fr:spring/wp7_ari/people_facts.git

#RUN git clone --depth 1 git@gitlab:interaction/expressive_eyes.git 

########################################################################
# BUILD STAGE 2 - copy the compiled app dir into a fresh runtime image
#FROM registry.gitlab.inria.fr/spring/dockers/spring-base as spring-base
FROM ros:noetic-ros-base

ARG ROS_DISTRO=noetic


LABEL maintainer "severin.lemaignan@pal-robotics.com"

SHELL ["/bin/bash", "-c"]

# same UID as INRIA -- probably not important
ARG UID=664534
RUN useradd -m -u $UID ros
RUN chown -R ros:ros /home/ros

# add ros user to sudoers
RUN usermod -aG sudo ros
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
USER ros

RUN sudo apt update && sudo apt install -y python3-pip
RUN sudo apt install -y python3-catkin-tools
RUN sudo apt install -y python3-scipy 

RUN pip3 install skia-python

RUN pip3 install rdflib reasonable==0.1.59

RUN sudo apt install -y ros-noetic-tf ros-noetic-cv-bridge
#####################################################################
### PREPARE THE ros4hri_ws WORKSPACE

ARG WS=/home/ros/ros4hri_ws

RUN mkdir -p ${WS}/src

###
### Get all the ROS4HRI nodes
#COPY --chown=ros:ros --from=sources /hri_fullbody ${WS}/src/hri_fullbody
#COPY --chown=ros:ros --from=sources /hri_face_detect ${WS}/src/hri_face_detect
#COPY --chown=ros:ros --from=sources /hri_face_identification ${WS}/src/hri_face_identification

COPY --chown=ros:ros --from=sources /hri ${WS}/src/hri
COPY --chown=ros:ros --from=sources /knowledge_core ${WS}/src/knowledge_core
COPY --chown=ros:ros --from=sources /people_facts ${WS}/src/people_facts
#COPY --chown=ros:ros modules/hri ${WS}/src/hri
#COPY --chown=ros:ros modules/hri_msgs ${WS}/src/hri_msgs
#COPY --chown=ros:ros modules/hri_actions_msgs ${WS}/src/hri_actions_msgs

#####################################################################
### INSTALL DEPS

WORKDIR ${WS}
RUN rosdep update
RUN sudo apt-get update && DEBIAN_FRONTEND=noninteractive rosdep install --from-paths src --ignore-src -y

COPY --chown=ros:ros --from=sources /hri_person_manager ${WS}/src/hri_person_manager
#####################################################################
### compile it all

RUN source /opt/ros/${ROS_DISTRO}/setup.sh && catkin config --install && catkin build

COPY entrypoint.sh /home/ros
RUN sudo chown ros:ros ~/entrypoint.sh
RUN chmod +x ~/entrypoint.sh
RUN mkdir -p /home/ros/.ros
RUN sudo chown -R ros:ros /home/ros/.ros

COPY launch/* /home/ros/

ENTRYPOINT ["/home/ros/entrypoint.sh"]
CMD ["roslaunch", "ros4hri.launch"]



