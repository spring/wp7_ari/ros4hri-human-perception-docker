#!/bin/bash
set -e

# setup ros environment
source /opt/ros/noetic/setup.bash

WS=/home/ros/ros4hri_ws

# if we have custom-compiled nodes, source the corresponding WS
if [ -d "$WS/install/" ]
then
    source $WS/install/setup.bash
fi

ORANGE='\033[0;33m'
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

echo -e "${ORANGE}"
echo "   ____   _    _               _           _   _               "
echo "  |  _ \ / \  | |    _ __ ___ | |__   ___ | |_(_) ___ ___      "
echo "  | |_) / _ \ | |   | '__/ _ \| '_ \ / _ \| __| |/ __/ __|     "
echo "  |  __/ ___ \| |___| | | (_) | |_) | (_) | |_| | (__\__ \     "
echo "  |_| /_/   \_\_____|_|  \___/|_.__/ \___/ \__|_|\___|___/     "
echo "                                                               "
echo -e "${GREEN}"
echo " "
echo "  ____  ____  ____  ___ _   _  ____  "
echo " / ___||  _ \|  _ \|_ _| \ | |/ ___| "
echo " \___ \| |_) | |_) || ||  \| | |  _  "
echo "  ___) |  __/|  _ < | || |\  | |_| | "
echo " |____/|_|   |_| \_\___|_| \_|\____| "
echo " "
echo -e "${NC}"
echo " "

cd ~

echo ""
echo ""

echo "➡️  Available launch files in the Docker image:"

for f in *.launch; 
do
    echo "- $f"; 
done
echo ""
echo "(add 'roslaunch <file.launch>' at the end of the docker start cmd to select)"
echo ""
echo ""

echo "🏁 Starting launch file: $@ "
echo ""

exec $@
